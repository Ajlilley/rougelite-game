﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {

    public static float player_health;
    float start_health = 100;

    public static float stamina;

    public static float player_attack_power;
    public static float player_defense_power;

    float base_attack = 15;
    float base_defense = 5;

    Image hp_bar;
    Text hp_text;

    Image stamina_bar;
    Text stamina_text;

    Text player_stats;

    GameObject game_over_screen;

	// Use this for initialization
	void Start () {

        player_health = 100;
        stamina = 100;

        player_attack_power = base_attack + StoredItems_EquippedItems.weapon_bonus;
        player_defense_power = base_defense + StoredItems_EquippedItems.total_armor_bonus;

        hp_bar = GameObject.Find("hp_bar").GetComponent<Image>();
        hp_text = GameObject.Find("hp_text").GetComponent<Text>();

        stamina_bar = GameObject.Find("stamina_bar").GetComponent<Image>();
        stamina_text = GameObject.Find("stamina_text").GetComponent<Text>();

        player_stats = GameObject.Find("player_stats").GetComponent<Text>();

        game_over_screen = GameObject.Find("GameOver");
        game_over_screen.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {

        player_attack_power = base_attack + StoredItems_EquippedItems.weapon_bonus;
        player_defense_power = base_defense + StoredItems_EquippedItems.total_armor_bonus;

        hp_bar.fillAmount = player_health / start_health;
        hp_text.text = player_health + "/" + start_health;

        stamina_bar.fillAmount = stamina / 100;
        stamina_text.text = stamina + "/" + "100";

        player_stats.text =
            "Attack: " + player_attack_power + "\n" +
             "Defense: " + player_defense_power + "\n" +
              "Helm: " + StoredItems_EquippedItems.Helm + "\n" +
              "Chest: " + StoredItems_EquippedItems.Chest + "\n" +
              "Gloves: " + StoredItems_EquippedItems.Gloves + "\n" +
              "Boots: " + StoredItems_EquippedItems.Boots + "\n" +
              "Weapon: " + StoredItems_EquippedItems.Weapon;

        if (player_health <= 0)
        {
            game_over_screen.SetActive(true);
        }

    }
}
