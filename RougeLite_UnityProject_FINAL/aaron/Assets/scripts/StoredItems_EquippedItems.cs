﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoredItems_EquippedItems : MonoBehaviour {

    public static float potions;
    Text Potion_Count;
    Text Potion_Count_2;

    public static float Bronze_Sword;
    public static float Iron_Sword;
    public static float Steel_Sword;
    public static float Epic_Sword;

    public static float Bronze_Helm;
    public static float Steel_Helm;
    public static float Steel_Chest;
    public static float Bronze_Boots;
    public static float Steel_Boots;
    public static float Steel_Gloves;

    public static string Helm;
    public static string Chest;
    public static string Gloves;
    public static string Boots;
    public static string Weapon;

    public static Text Weapons;
    public static Text Armor;
    public static Text Equipped;

    public static Text Total;

    public static float total_items;

    public static float total_armor_bonus;
    public static float weapon_bonus;

    public static float armor_helm_bonus;
    public static float armor_chest_bonus;
    public static float armor_glove_bonus;
    public static float armor_boot_bonus;

    // Use this for initialization
    void Start () {

        Helm = "";
        Chest = "";
        Boots = "";


        total_items = 0;

        potions = 1;

        Potion_Count = GameObject.Find("potion_text").GetComponent<Text>();
        Potion_Count_2 = GameObject.Find("potion_text_2").GetComponent<Text>();

        Bronze_Sword = 1;
        Iron_Sword = 0;
        Steel_Sword = 0;
        Epic_Sword = 0;
        Bronze_Helm = 0;
        Steel_Helm = 0;
        Steel_Chest = 0;
        Bronze_Boots = 0;
        Steel_Boots = 0;
        Steel_Gloves = 1;

        Weapon = "Bronze Sword";

        Gloves = "Steel Gloves";


    }
	
	// Update is called once per frame
	void Update () {

        Potion_Count.text = "Use Potion" + "\n" + "Heals: 25" + "\n" + "x" + potions + " left";
        Potion_Count_2.text = "Use Potion" + "\n" + "Heals: 25" + "\n" + "x" + potions + " left";

        total_items = 

        Bronze_Sword +
        Iron_Sword +
        Steel_Sword +
        Epic_Sword +
        Bronze_Helm +
        Steel_Helm +
        Steel_Chest +
        Bronze_Boots +
        Steel_Boots +
        Steel_Gloves;

        if (combat.post_combat_buttons.activeInHierarchy)
        {
            Weapons = GameObject.Find("Weapon_Items").GetComponent<Text>();
            Armor = GameObject.Find("Armor_Items").GetComponent<Text>();
            Equipped = GameObject.Find("Equipped_Items").GetComponent<Text>();


            Weapons.text = "WEAPONS:" + "\n" +
                "Bronze Sword x " + Bronze_Sword + "\n" +
                "Iron Sword x " + Iron_Sword + "\n" +
                "Steel Sword x " + Steel_Sword + "\n" +
                "Epic Sword x " + Epic_Sword;

            Armor.text = "ARMOR:" + "\n" +
                "Bronze Helm x " + Bronze_Helm + "\n" +
                "Steel Helm x " + Steel_Helm + "\n" +
                "Steel Chest x " + Steel_Chest + "\n" +
                "Bronze Boots x " + Bronze_Boots + "\n" +
                "Steel Boots x " + Steel_Boots + "\n" +
                "Steel Gloves x " + Steel_Gloves;

            Equipped.text = "EQUIPPED:" + "\n" +
                "Helm: " + Helm + "\n" +
                "Chest: " + Chest + "\n" +
                "Gloves: " + Gloves + "\n" +
                "Boots: " + Boots + "\n" +
                "Weapon: " + Weapon;

            Total = GameObject.Find("TOTAL_ITEMS").GetComponent<Text>();
            Total.text = "TOTAL: " + total_items + "/" + "20";

        }

       

        ////////// WEAPONS

        if (Weapon == "Bronze Sword")
        {
            weapon_bonus = 2;
        }
        if (Weapon == "Iron Sword")
        {
            weapon_bonus = 4;
        }
        if (Weapon == "Steel Sword")
        {
            weapon_bonus = 6;
        }
        if (Weapon == "Epic Sword")
        {
            weapon_bonus = 10;
        }

        ///////// ARMOR

        if (Helm == "Bronze Helm")
        {
            armor_helm_bonus = 1;
        }
        if (Helm == "Steel Helm")
        {
            armor_helm_bonus = 2;
        }

        if (Chest == "Steel Chest")
        {
            armor_chest_bonus = 4;
        }

        if (Boots == "Bronze Boots")
        {
            armor_boot_bonus = 1;
        }
        if (Boots == "Steel Boots")
        {
            armor_boot_bonus = 2;
        }

        if (Gloves == "Steel Gloves")
        {
            armor_glove_bonus = 2;
        }

        total_armor_bonus = armor_boot_bonus + armor_chest_bonus + armor_glove_bonus + armor_helm_bonus;
    }

    public void equip_bronze_sword()
    {
        if (Bronze_Sword > 0)
        {
            Weapon = "Bronze Sword";
        }
    }
    public void destroy_bronze_sword()
    {
        if (Bronze_Sword > 0)
        {
            Bronze_Sword -= 1;
        }
    }





    public void equip_iron_sword()
    {
        if (Iron_Sword > 0)
        {
            Weapon = "Iron Sword";
        }
    }
    public void destroy_iron_sword()
    {
        if (Iron_Sword > 0)
        {
            Iron_Sword -= 1;
        }
    }




    public void equip_steel_sword()
    {
        if (Steel_Sword > 0)
        {
            Weapon = "Steel Sword";
        }
    }
    public void destroy_steel_sword()
    {
        if (Steel_Sword > 0)
        {
            Steel_Sword -= 1;
        }
    }




    public void equip_epic_sword()
    {
        if (Epic_Sword > 0)
        {
            Weapon = "Epic Sword";
        }
    }
    public void destroy_epic_sword()
    {
        if (Epic_Sword > 0)
        {
            Epic_Sword -= 1;
        }
    }





    public void equip_bronze_helm()
    {
        if (Bronze_Helm > 0)
        {
            Helm = "Bronze Helm";
        }
    }
    public void destroy_bronze_helm()
    {
        if (Bronze_Helm > 0)
        {
            Bronze_Helm --;
        }
    }




    public void equip_steel_helm()
    {
        if (Steel_Helm > 0)
        {
            Helm = "Steel Helm";
        }
    }
    public void destroy_steel_helm()
    {
        if (Steel_Helm > 0)
        {
            Steel_Helm--;
        }
    }




    public void equip_steel_chest()
    {
        if (Steel_Chest > 0)
        {
            Chest = "Steel Chest";
        }
    }
    public void destroy_steel_chest()
    {
        if (Steel_Chest > 0)
        {
            Steel_Chest--;
        }
    }





    public void equip_steel_boots()
    {
        if (Steel_Boots > 0)
        {
            Boots = "Steel Boots";
        }
    }
    public void destroy_steel_boots()
    {
        if (Steel_Boots > 0)
        {
            Steel_Boots--;
        }
    }




    public void equip_bronze_boots()
    {
        if (Bronze_Boots > 0)
        {
            Boots = "Bronze Boots";
        }
    }
    public void destroy_bronze_boots()
    {
        if (Bronze_Boots > 0)
        {
            Bronze_Boots--;
        }
    }





    public void equip_steel_gloves()
    {
        if (Steel_Gloves > 0)
        {
            Gloves = "Steel Gloves";
        }
    }
    public void destroy_steel_gloves()
    {
        if (Steel_Gloves > 0)
        {
            Steel_Gloves--;
        }
    }


}
