﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Start_Game()
    {
        SceneManager.LoadScene("main", LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
