﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class combat : MonoBehaviour {

    public static float Boss_Counter = 1;

    bool player_turn;
    bool enemy_attacking;

    GameObject player_turn_indicator;
    GameObject enemy_turn_indicator;

    Text combat_text;
    Text battle_count;

    public static float battle_number;

    bool ok_to_attack;

    GameObject spawned_enemy;
    Vector3 enemy_position;

    public static GameObject post_combat_buttons;

    float enemy_health;

    public static bool fight_over;

    int random_potion_drop;
    int gained_potions;

    int TemporarilyDeactivate;

    public static string gained_item;

    IEnumerator Player_Attack()
    {
        yield return new WaitForSeconds(0);

        if (Character.player_attack_power - enemy.total_defense >= 0)
        {
            enemy.enemy_health -= Character.player_attack_power - enemy.total_defense;
        }

        combat_text.text = "Player Dealt " + (Character.player_attack_power - enemy.total_defense) + " Damage";

        yield return new WaitForSeconds(2);

        player_turn = false;
        player_turn_indicator.SetActive(false);
    }

    IEnumerator Player_Heavy_Attack()
    {
        yield return new WaitForSeconds(0);

        if (Character.player_attack_power - enemy.total_defense >= 0)
        {
            enemy.enemy_health -= (Character.player_attack_power - enemy.total_defense)*2;
        }

        combat_text.text = "Player Dealt " + (Character.player_attack_power - enemy.total_defense)*2 + " Damage";

        yield return new WaitForSeconds(2);

        player_turn = false;
        player_turn_indicator.SetActive(false);
    }

    IEnumerator Enemy_Attack()
    {
        player_turn_indicator.SetActive(false);
        enemy_turn_indicator.SetActive(true);

        yield return new WaitForSeconds(2);

        if (enemy.total_attack - Character.player_defense_power >= 0 && spawned_enemy != null)
        {
            Character.player_health -= enemy.total_attack - Character.player_defense_power;

            if (!this.GetComponent<AudioSource>().isPlaying)
            {
                this.GetComponent<AudioSource>().Play();
            }
        }
        if (spawned_enemy != null)
        {
            combat_text.text = "Enemy Dealt " + (enemy.total_attack - Character.player_defense_power) + " Damage";
        }

        yield return new WaitForSeconds(1);

        player_turn = true;
        enemy_attacking = false;

        enemy_turn_indicator.SetActive(false);
        player_turn_indicator.SetActive(true);

        ok_to_attack = true;

    }

    // Use this for initialization
    void Start () {

        Boss_Counter = 1;

        player_turn = true;
        enemy_attacking = false;

        ok_to_attack = true;

        player_turn_indicator = GameObject.Find("player_turn");
        enemy_turn_indicator = GameObject.Find("enemy_turn");

        enemy_turn_indicator.SetActive(false);

        combat_text = GameObject.Find("combat_text").GetComponent<Text>();
        post_combat_buttons = GameObject.Find("POST_COMBAT_BUTTONS");
        post_combat_buttons.SetActive(false);

        battle_count = GameObject.Find("Battle_Count").GetComponent<Text>();

        battle_number = 1;

        spawned_enemy = GameObject.FindGameObjectWithTag("enemy");
        enemy_position = spawned_enemy.transform.position;

        fight_over = false;
    }
	
	// Update is called once per frame
	void Update () {

        battle_count.text = "FIGHT: " + battle_number;

        spawned_enemy = GameObject.FindGameObjectWithTag("enemy");

        enemy_health = enemy.enemy_health;

        if(enemy_health <= 0 && spawned_enemy != null && fight_over == false)
        {
            fight_over = true;
            Destroy(spawned_enemy.gameObject);

            enemy.hp_bar.fillAmount = 0;
            enemy.hp_text.text = "0" + "/" + enemy.start_health;

            Boss_Counter++;

            random_potion_drop = Random.Range(1, 5);

            if (random_potion_drop == 2 && StoredItems_EquippedItems.potions <2)
            {
                StoredItems_EquippedItems.potions++;
                gained_potions = 1;
            }
            if (random_potion_drop == 2 && StoredItems_EquippedItems.potions >= 2)
            {
                gained_potions = 0;
            }
            else if (random_potion_drop != 2)
            {
                gained_potions = 0;
            }
        }
        if (spawned_enemy == null && fight_over == true)
        {

            combat_text.text = "Enemy Defeated!" + "\n" + "You Gained " + gained_potions + " Potions" + "\n"
                + "Items Looted: " + gained_item;

            post_combat_buttons.SetActive(true);
        }

        if(player_turn == false && enemy_attacking == false && spawned_enemy != null)
        {
            StartCoroutine("Enemy_Attack");
            enemy_attacking = true;
        }

    }

    public void AttackButton()
    {
        if (player_turn == true && ok_to_attack == true && spawned_enemy != null)
        {
            StartCoroutine("Player_Attack");
            player_turn = false;
            ok_to_attack = false;

            if (!this.GetComponent<AudioSource>().isPlaying)
            {
                this.GetComponent<AudioSource>().Play();
            }
        }
    }

    public void HeavyAttackButton()
    {
        if (player_turn == true && ok_to_attack == true && spawned_enemy != null && Character.stamina >= 20)
        {
            Character.stamina -= 20;
            StartCoroutine("Player_Heavy_Attack");
            player_turn = false;
            ok_to_attack = false;

            if (!this.GetComponent<AudioSource>().isPlaying)
            {
                this.GetComponent<AudioSource>().Play();
            }
        }
    }


}
