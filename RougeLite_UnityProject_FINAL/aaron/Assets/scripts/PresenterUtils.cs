﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PresenterUtils : MonoBehaviour {

    Vector3 enemy_position;
    GameObject post_combat_buttons;

    GameObject inventory_screen;

	// Use this for initialization
	void Start () {

        enemy_position = GameObject.FindGameObjectWithTag("enemy").transform.position;

        post_combat_buttons = GameObject.Find("POST_COMBAT_BUTTONS");
    }
	
	// Update is called once per frame
	void Update () {

       
	}

    public void NextFight()
    {

        Instantiate(Resources.Load("enemy") as GameObject, enemy_position, Quaternion.identity);

        combat.battle_number++;

        post_combat_buttons.SetActive(false);

    }

    public void UsePotion()
    {
        if(StoredItems_EquippedItems.potions > 0 && Character.player_health > 0)
        {
            Character.player_health += 25f;
            if(Character.player_health > 100)
            {
                Character.player_health = 100;
            }
            StoredItems_EquippedItems.potions--;
        }

    }

    public void ReutrnMain()
    {
        SceneManager.LoadScene("01_title");
    }
}
