﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemy : MonoBehaviour {

    public static float enemy_health;

    public static float start_health;

    string enemy_type;

    float enemy_base_attack;
    float enemy_base_defense;
    float weapon_bonus;
    float armor_bonus;

    public static float total_attack;
    public static float total_defense;

    string weapon;
    string armor;

    int random_enemy;

    int random_armor;
    int random_weapon;


    public static Image hp_bar;
    public static Text hp_text;

    Text enemy_stats;


    int random_bonus_item;

    int random_wizard_bonus_1;
    int random_wizard_bonus_2;

    bool item_drop;

    public Sprite rat;
    public Sprite goblin;
    public Sprite orc;
    public Sprite wizard;

    SpriteRenderer enemy_sprite;
 

    // Use this for initialization
    void Start () {

        enemy_sprite = this.GetComponent<SpriteRenderer>();


        combat.gained_item = "";

        item_drop = false;

        random_bonus_item = Random.Range(1,11);

        random_wizard_bonus_1 = 0;
        random_wizard_bonus_2 = 0;

        combat.fight_over = false;

        hp_bar = GameObject.Find("enemy_hp_bar").GetComponent<Image>();
        hp_text = GameObject.Find("enemy_hp_text").GetComponent<Text>();

        enemy_stats = GameObject.Find("enemy_stats").GetComponent<Text>();

        if (combat.Boss_Counter % 6 != 0)
        {

            random_enemy = Random.Range(1, 4);

            if (random_enemy == 1)
            {
                enemy_type = "Rat";

                enemy_sprite.sprite = rat;

                enemy_health = Random.Range(15, 21);
                start_health = enemy_health;

                enemy_base_attack = Random.Range(10, 12);
                enemy_base_defense = Random.Range(2, 4);
            }
            else if (random_enemy == 2)
            {
                enemy_type = "Orc";

                enemy_sprite.sprite = orc;

                enemy_health = Random.Range(30, 41);
                start_health = enemy_health;

                enemy_base_attack = Random.Range(12, 15);
                enemy_base_defense = Random.Range(4, 6);
            }
            else if (random_enemy == 3)
            {
                enemy_type = "Goblin";

                enemy_sprite.sprite = goblin;

                enemy_health = Random.Range(40, 51);
                start_health = enemy_health;

                enemy_base_attack = Random.Range(13, 16);
                enemy_base_defense = Random.Range(6, 8);
            }


        }

        if (combat.Boss_Counter % 6 == 0)
        {
            random_wizard_bonus_1 = Random.Range(1, 11);
            random_wizard_bonus_2 = Random.Range(1, 11);

            //StoredItems_EquippedItems.potions++;
            
            enemy_type = "BOSS WIZARD";

            enemy_sprite.sprite = wizard;

            enemy_health = Random.Range(60, 71);
            start_health = enemy_health;

            enemy_base_attack = Random.Range(20, 25);
            enemy_base_defense = Random.Range(8, 12);

          

        }


            ///////////////////////////////////////////////////// ARMOR

        random_armor = Random.Range(1, 7);

        if (random_armor == 1)
        {
            armor = "Bronze Helm";  

            armor_bonus = 1;
        }
        if (random_armor == 2)
        {
            armor = "Steel Helm";

            armor_bonus = 2;
        }
        if (random_armor == 3)
        {
            armor = "Steel Chest";
   
            armor_bonus = 4;
        }
        if (random_armor == 4)
        {
            armor = "Bronze Boots";     

            armor_bonus = 1;
        }
        if (random_armor == 5)
        {
            armor = "Steel Boots";

            armor_bonus = 2;
        }
        if (random_armor == 6)
        {
            armor = "Steel Gloves";

            armor_bonus = 2;
        }

        ///////////////////////////////////////////////////// Weapon

        random_weapon = Random.Range(1, 5);

        if (random_weapon == 1)
        {
            weapon = "Bronze Sword";

            weapon_bonus = 2;
        }
        if (random_weapon == 2)
        {
            weapon = "Iron Sword";

            weapon_bonus = 4;
        }
        if (random_weapon == 3)
        {
            weapon = "Steel Sword";  

            weapon_bonus = 6;
        }
        if (random_weapon == 4)
        {
            weapon = "Epic Sword";

            weapon_bonus = 10;
        }

        total_attack = enemy_base_attack + weapon_bonus;
        total_defense = enemy_base_defense + armor_bonus;
    }
	
	// Update is called once per frame
	void Update () {

        hp_bar.fillAmount = enemy_health / start_health;
        hp_text.text = enemy_health + "/" + start_health;

        enemy_stats.text =
          "Type: " + enemy_type + "\n" +
           "Base Attack: " + enemy_base_attack + "\n" +
           "Total Attack: " + total_attack + "\n" +
            "Base Defense: " + enemy_base_defense + "\n" +
             "Total Defense: " + total_defense + "\n" +
             "Wep: " + weapon + "\n" +
             "Armor: " + armor;


        if (item_drop == false && StoredItems_EquippedItems.total_items < 19)
        {

            if (random_bonus_item == 1)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Sword++;

                    combat.gained_item += "Bronze Sword, ";
                }
            }
            if (random_bonus_item == 2)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Iron_Sword++;

                    combat.gained_item += "Iron Sword, ";
                }
            }
            if (random_bonus_item == 3)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Sword++;

                    combat.gained_item += "Steel Sword, ";
                }
            }
            if (random_bonus_item == 4)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Epic_Sword++;

                    combat.gained_item += "Epic Sword, ";
                }
            }
            if (random_bonus_item == 5)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Helm++;

                    combat.gained_item += "Bronze Helm, ";
                }
            }
            if (random_bonus_item == 6)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Helm++;

                    combat.gained_item += "Steel Helm, ";
                }
            }
            if (random_bonus_item == 7)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Chest++;

                    combat.gained_item += "Steel Chest, ";
                }
            }
            if (random_bonus_item == 8)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Boots++;

                    combat.gained_item += "Bronze Boots, ";
                }
            }
            if (random_bonus_item == 9)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Boots++;

                    combat.gained_item += "Steel Boots, ";
                }

            }
            if (random_bonus_item == 10)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Gloves++;

                    combat.gained_item += "Steel Gloves, ";
                }

            }
            //////////////////////////////////////// BOSS
            if (random_wizard_bonus_1 == 1)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Sword++;

                    combat.gained_item += "Bronze Sword, ";
                }

            }
            if (random_wizard_bonus_1 == 2)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Iron_Sword++;

                    combat.gained_item += "Iron Sword, ";
                }

            }
            if (random_wizard_bonus_1 == 3)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Sword++;

                    combat.gained_item += "Steel Sword, ";
                }

            }
            if (random_wizard_bonus_1 == 4)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Epic_Sword++;

                    combat.gained_item += "Epic Sword, ";
                }

            }
            if (random_wizard_bonus_1 == 5)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Helm++;

                    combat.gained_item += "Bronze Helm, ";
                }

            }
            if (random_wizard_bonus_1 == 6)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Helm++;

                    combat.gained_item += "Steel Helm, ";
                }

            }
            if (random_wizard_bonus_1 == 7)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Chest++;

                    combat.gained_item += "Steel Chest, ";
                }

            }
            if (random_wizard_bonus_1 == 8)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Boots++;

                    combat.gained_item += "Bronze Boots, ";
                }

            }
            if (random_wizard_bonus_1 == 9)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Boots++;

                    combat.gained_item += "Steel Boots, ";
                }

            }
            if (random_wizard_bonus_2 == 10)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Gloves++;

                    combat.gained_item += "Steel Gloves, ";
                }

            }

            if (random_wizard_bonus_2 == 1)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Sword++;

                    combat.gained_item += "Bronze Sword, ";
                }

            }
            if (random_wizard_bonus_2 == 2)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Iron_Sword++;

                    combat.gained_item += "Iron Sword, ";
                }

            }
            if (random_wizard_bonus_2 == 3)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Sword++;

                    combat.gained_item += "Steel Sword, ";
                }

            }
            if (random_wizard_bonus_2 == 4)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Epic_Sword++;

                    combat.gained_item += "Epic Sword, ";
                }

            }
            if (random_wizard_bonus_2 == 5)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Helm++;

                    combat.gained_item += "Bronze Helm, ";
                }

            }
            if (random_wizard_bonus_2 == 6)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Helm++;

                    combat.gained_item += "Steel Helm, ";
                }

            }
            if (random_wizard_bonus_2 == 7)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Chest++;

                    combat.gained_item += "Steel Chest, ";
                }

            }
            if (random_wizard_bonus_2 == 8)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Bronze_Boots++;

                    combat.gained_item += "Bronze Boots, ";
                }

            }
            if (random_wizard_bonus_2 == 9)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Boots++;

                    combat.gained_item += "Steel Boots, ";
                }

            }
            if (random_wizard_bonus_2 == 10)
            {
                if (StoredItems_EquippedItems.total_items < 19)
                {
                    StoredItems_EquippedItems.Steel_Gloves++;

                    combat.gained_item += "Steel Gloves, ";
                }

            }




            if (random_weapon == 1)
            {

                StoredItems_EquippedItems.Bronze_Sword++;

                combat.gained_item += "Bronze Sword, ";

            }
            if (random_weapon == 2)
            {

                StoredItems_EquippedItems.Iron_Sword++;

                combat.gained_item += "Iron Sword, ";

            }
            if (random_weapon == 3)
            {

                StoredItems_EquippedItems.Steel_Sword++;

                combat.gained_item += "Steel Sword, ";

            }
            if (random_weapon == 4)
            {

                StoredItems_EquippedItems.Epic_Sword++;

                combat.gained_item += "Epic Sword, ";

            }

            if (random_armor == 1)
            {

                StoredItems_EquippedItems.Bronze_Helm++;

                combat.gained_item += "Bronze Helm, ";

            }
            if (random_armor == 2)
            {

                StoredItems_EquippedItems.Steel_Helm++;

                combat.gained_item += "Steel Helm, ";

            }
            if (random_armor == 3)
            {

                StoredItems_EquippedItems.Steel_Chest++;

                combat.gained_item += "Steel Chest, ";

            }
            if (random_armor == 4)
            {

                StoredItems_EquippedItems.Bronze_Boots++;

                combat.gained_item += "Bronze Boots, ";

            }
            if (random_armor == 5)
            {

                StoredItems_EquippedItems.Steel_Boots++;

                combat.gained_item += "Steel Boots, ";

            }
            if (random_armor == 6)
            {

                StoredItems_EquippedItems.Steel_Gloves++;

                combat.gained_item += "Steel Gloves, ";

            }

            item_drop = true;
        }

    }
}
